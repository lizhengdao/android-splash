package com.davidtiago.androidsplash.randompicture.data

import com.davidtiago.androidsplash.randompicture.data.remote.RandonPictureService
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.TestCoroutineScope
import kotlinx.coroutines.test.runBlockingTest
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe
import kotlin.test.assertEquals

@ExperimentalCoroutinesApi
object RemoteRandomPictureRepositorySpec : Spek({
    val testScope by memoized { TestCoroutineScope() }
    val fakeRandomPictureService by memoized { FakeRandomPictureService(testScope) }
    val randomPictureRepository by memoized { RemoteRandomPictureRepository(fakeRandomPictureService) }
    lateinit var randomPicture: RandomPicture
    describe("Invokes service") {
        beforeEachTest {
            testScope.runBlockingTest {
                randomPicture = randomPictureRepository.getRandomPicture()
            }
        }
        afterEachTest { testScope.cleanupTestCoroutines() }
        it("should invoke repository") {
            assertEquals(
                expected = 1,
                actual = fakeRandomPictureService.invokeCount
            )
        }
        it("should have descpription") {
            assertEquals(
                expected = "Random picture",
                actual = randomPicture.description
            )
        }
    }
})

private class FakeRandomPictureService(
    private val scope: CoroutineScope
) : RandonPictureService {
    var invokeCount = 0
    override suspend fun get(): RandomPicture {
        scope.launch {
            invokeCount++
            delay(1000) // Simulates network delay
        }.join()
        return RandomPicture(description = "Random picture")
    }
}
