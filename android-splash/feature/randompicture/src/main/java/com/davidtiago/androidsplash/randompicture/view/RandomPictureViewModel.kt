package com.davidtiago.androidsplash.randompicture.view

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.davidtiago.androidsplash.randompicture.data.RandomPicture
import com.davidtiago.androidsplash.randompicture.data.RandomPictureRepository

class RandomPictureViewModel @ViewModelInject constructor(
    private val repository: RandomPictureRepository,
    @Assisted private val savedStateHandle: SavedStateHandle
) : ViewModel() {
    val viewState = liveData {
        emit(RandomPictureViewState.Loading)
        val state = RandomPictureViewState.Loaded(
            repository.getRandomPicture()
        )
        emit(state)
    }
}

sealed class RandomPictureViewState {
    class Loaded(val randomPicture: RandomPicture) : RandomPictureViewState()
    object Loading : RandomPictureViewState()
    object LoadError : RandomPictureViewState()
}
