package com.davidtiago.androidsplash.randompicture.di

import com.davidtiago.androidsplash.randompicture.BuildConfig
import com.davidtiago.androidsplash.randompicture.data.remote.RandonPictureService
import com.davidtiago.androidsplash.randompicture.network.RetrofitClientCreator
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import retrofit2.Retrofit
import javax.inject.Named

@Module
@InstallIn(ApplicationComponent::class)
object RandomPictureModule {
    @Provides
    @Named("apiPublicKey")
    fun provideApiPublicKey(): String = BuildConfig.API_ACCESS_KEY

    @Provides
    @Named("baseUrl")
    fun provideBseUrl(): String = "https://api.unsplash.com/"

    @Provides
    fun provideRetrofit(retrofitClientCreator: RetrofitClientCreator): Retrofit =
        retrofitClientCreator.create()

    @Provides
    fun provideRandomPictureService(retrofit: Retrofit): RandonPictureService =
        retrofit.create(RandonPictureService::class.java)
}
